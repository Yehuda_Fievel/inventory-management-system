const Order = require('../models/orders');


async function create(req, res, next) {
    try {
        const order = await Order.create(req.body);
        res.status(201).json({ response: order, message: 'order created' });
    } catch (error) {
        next(error);
    }
}


async function get(req, res, next) {
    try {
        const query = {};
        if (req.query.to) {
            query.createdAt = {
                $gte: req.query.from,
                $lte: req.query.to,
            }
        }
        const find = Order.find(query).populate('product');
        if (req.query.skip > 0) {
            find.skip(parseInt(req.query.skip));
        }
        if (req.query.limit > 0) {
            find.limit(parseInt(req.query.limit));
        }
        if (req.query.sort) {
            find.sort(req.query.sort);
        }
        const [orders, count] = await Promise.all([find, Order.countDocuments(query)]);
        res.status(200).json({ response: { orders, count } });
    } catch (error) {
        next(error)
    }
}


module.exports = {
    create,
    get,
}