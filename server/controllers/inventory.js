const Inventory = require('../models/inventory');
const Request = require('../models/requestInventory');


async function create(req, res, next) {
    try {
        const inventory = await Inventory.create(req.body);
        res.status(201).json({ response: inventory, message: 'inventory created' });
    } catch (error) {
        next(error);
    }
}


async function get(req, res, next) {
    try {
        const query = {};
        if (req.query.to) {
            query.createdAt = {
                $gte: req.query.from,
                $lte: req.query.to,
            }
        }
        const find = Inventory.find(query);
        if (req.query.skip > 0) {
            find.skip(parseInt(req.query.skip));
        }
        if (req.query.limit > 0) {
            find.limit(parseInt(req.query.limit));
        }
        if (req.query.sort) {
            find.sort(req.query.sort);
        }
        const [inventory, count] = await Promise.all([find, Inventory.countDocuments(query)]);
        res.status(200).json({ response: { inventory, count } });
    } catch (error) {
        next(error)
    }
}


async function getOne(req, res, next) {
    try {
        const inventory = await Inventory.findOne({ _id: req.params.id });
        res.status(200).json({ response: inventory });
    } catch (error) {
        next(error);
    }
}


async function update(req, res, next) {
    try {
        switch (req.user.role) {
            case 'sales':
                await Promise.all([
                    Inventory.updateOne({ _id: req.params.id }, { inStock: false }),
                    Request.create({ product: req.params.id }),
                ]);
                res.status(200).json({ message: 'request sent' });
                break;
            case 'logistics':
                await Promise.all([
                    Inventory.updateOne({ _id: req.params.id }, { inStock: true }),
                    Request.deleteOne({ product: req.params.id }),
                ]);
                res.status(200).json({ message: 'inventory updated' });
                break;
            default:
                res.sendStatus(400);
                break;
        }
    } catch (error) {
        next(error);
    }
}


module.exports = {
    create,
    get,
    getOne,
    update,
}