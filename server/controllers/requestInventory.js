const Request = require('../models/requestInventory');


async function get(req, res, next) {
    try {
        const find = Request.find().populate('product');
        if (req.query.skip > 0) {
            find.skip(parseInt(req.query.skip));
        }
        if (req.query.limit > 0) {
            find.limit(parseInt(req.query.limit));
        }
        if (req.query.sort) {
            find.sort(req.query.sort);
        }
        const [requestInventory, count] = await Promise.all([find, Request.countDocuments()]);
        res.status(200).json({ response: { requestInventory, count } });
    } catch (error) {
        next(error)
    }
}


module.exports = {
    get,
}