const jwt = require('jsonwebtoken');
const config = require('../config');

/**
 * Create JWT token
 * @param {object} user
 * @returns - jwt token
 */
function createJWT(user) {
    const sign = {
        iss: 'server',
        sub: user._id,
        role: user.role,
        iat: Date.now(),
    }
    return token = jwt.sign(sign, config.jwtSecret);
}


module.exports = {
    createJWT,
}