const express = require('express');
const app = express();
const cors = require('cors')
const bodyParser = require('body-parser');
const config = require('./config');
const port = process.env.PORT || config.port;


app.use(cors());
app.use(bodyParser.json())

require('./db');

require('./routes')(app);

process.on('unhandledRejection', error => {
    console.log('unhandledRejection: ' + error.message);
    console.log(error);
});

process.on('uncaughtException', (err, origin) => {
    console.log(`Exception origin: ${origin}`);
    console.log(`Caught exception: ${err}`);
});


app.listen(port, () => console.log(`App is listening on port ${port}!`));