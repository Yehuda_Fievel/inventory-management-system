module.exports = {
    connectionString: 'mongodb://mongo:27017/management',
    port: 3000,
    jwtSecret: 'secret',
}