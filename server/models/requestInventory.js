const mongoose = require('mongoose');


const RequestInventory = new mongoose.Schema({
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Inventory',
        required: true,
        unique: true,
    },
    isArrived: {
        type: Boolean,
        default: false,
    }
}, { timestamps: true });


module.exports = exports = mongoose.model('RequestInventory', RequestInventory, 'requests');