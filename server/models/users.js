const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');


const User = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        enum: ['sales', 'logistics'],
        required: true,
    },
}, { timestamps: true });

User.plugin(bcrypt);


module.exports = exports = mongoose.model('User', User, 'users');