const mongoose = require('mongoose');


const Order = new mongoose.Schema({
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Inventory',
    },
}, { timestamps: true });


module.exports = exports = mongoose.model('Order', Order, 'orders');