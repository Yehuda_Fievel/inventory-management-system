const mongoose = require('mongoose');


const Inventory = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    inStock: {
        type: Boolean,
        default: true,
    },
}, { timestamps: true });


module.exports = exports = mongoose.model('Inventory', Inventory, 'inventory');