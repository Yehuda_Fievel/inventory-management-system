const passport = require('passport');
const { Strategy, ExtractJwt } = require('passport-jwt');
const User = require('../models/users');
const config = require('../config');
const ErrorHandler = require('../utils/errorHandler');



passport.use(new Strategy({ jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), secretOrKey: config.jwtSecret }, async (jwt_payload, done) => {
    try {
        const user = await User.findOne({ _id: jwt_payload.sub });
        if (user) {
            return done(null, user);
        }
        return done(null, false);
    } catch (e) {
        return done(e, false);
    }
}));


function authenticateUser(req, res, next) {
    passport.authenticate('jwt', { session: false })(req, res, next);
}


function hasRole(...permittedRoles) {
    return (req, res, next) => {
        try {
            if (permittedRoles.includes(req.user.role)) {
                return next();
            }
            throw new ErrorHandler(403, 'user does not have proper permissions');
        } catch (error) {
            next(error)
        }
    }
}


module.exports = {
    authenticateUser,
    hasRole,
}