const middlweare = require('./middleware');

module.exports = function (app) {
    app.use('/users', require('./users'));
    app.use('/inventory', middlweare.authenticateUser, require('./inventory'));
    app.use('/orders', middlweare.authenticateUser, require('./orders'));
    app.use('/requestInventory', middlweare.authenticateUser, require('./requestInventory'));


     // Handles 404 request if no matching route to avoid sending back default express error
     app.use((req, res, next) => {
        const error = new Error('You do not belong here');
        console.log(error);
        res.status(404).send(error.message);
    });

    // Error Handler
    app.use((err, req, res, next) => {
        console.log(err);
        res.status(err.statusCode || 500).json({ error: true, message: err.message });
    });
}