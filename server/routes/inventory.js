const express = require('express');
const router = express.Router();
const middleware = require('./middleware');
const inventorySchema = require('../schema/inventory');
const inventoryCtrl = require('../controllers/inventory');


router.post('', middleware.hasRole('logistics'), inventorySchema.create, inventoryCtrl.create);

router.get('', middleware.hasRole('logistics', 'sales'), inventorySchema.get, inventoryCtrl.get);

// router.get('/:id', middleware.hasRole('sales'), inventoryCtrl.getOne);

router.patch('/:id/:type', middleware.hasRole('logistics', 'sales'), inventoryCtrl.update);


module.exports = router;