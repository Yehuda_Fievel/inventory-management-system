const express = require('express');
const router = express.Router();
const userSchema = require('../schema/users');
const userCtrl = require('../controllers/users');


router.post('/register', userSchema.register, userCtrl.register);

router.post('/login', userSchema.login, userCtrl.login);


module.exports = router;