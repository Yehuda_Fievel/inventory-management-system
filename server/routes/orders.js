const express = require('express');
const router = express.Router();
const middleware = require('./middleware');
const ordersSchema = require('../schema/orders');
const ordersCtrl = require('../controllers/orders');


router.post('', middleware.hasRole('sales'), ordersSchema.create, ordersCtrl.create);

router.get('', middleware.hasRole('sales'), ordersSchema.get, ordersCtrl.get);


module.exports = router;