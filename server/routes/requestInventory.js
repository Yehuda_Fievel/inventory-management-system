const express = require('express');
const router = express.Router();
const middleware = require('./middleware');
const inventorySchema = require('../schema/inventory');
const requestInventoryCtrl = require('../controllers/requestInventory');


router.get('', middleware.hasRole('logistics'), inventorySchema.get, requestInventoryCtrl.get);


module.exports = router;