const Joi = require('joi');


function validate({ schema, body, res, next }) {
    const { error, value } = schema.validate(body);
    if (error) {
        console.log(error)
        return res.status(400).json({ error: true, result: error.message });
    }
    next();
}


function register(req, res, next) {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        confirmPassword: Joi.string().valid(Joi.ref('password')).required(),
        role: Joi.string().required().valid('sales', 'logistics'),
    });
    validate({ schema, body: req.body, res, next });
}


function login(req, res, next) {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
    });
    validate({ schema, body: req.body, res, next });
}


module.exports = {
    register,
    login,
}