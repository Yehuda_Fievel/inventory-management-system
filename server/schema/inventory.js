const Joi = require('joi');


function validate({ schema, body, res, next }) {
    const { error, value } = schema.validate(body);
    if (error) {
        console.log(error);
        return res.status(400).json({ error: true, result: error.message });
    }
    next();
}


function create(req, res, next) {
    const schema = Joi.object().keys({
        name: Joi.string().required(),
    });
    validate({ schema, body: req.body, res, next });
}


function get(req, res, next) {
    const schema = Joi.object().keys({
        skip: Joi.string(),
        limit: Joi.string(),
        sort: Joi.string(),
        from: Joi.string(),
        to: Joi.string(),
    });
    validate({ schema, body: req.query, res, next });
}


module.exports = {
    create,
    get,
}