const mongoose = require('mongoose');
const config = require('./config');

mongoose.connect(config.connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}).catch(error => console.log(error));

mongoose.connection.on('connected', () => console.log('Mongo connection established successfully'));

mongoose.connection.on('error', err => console.log(err));