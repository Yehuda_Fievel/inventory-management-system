import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import ManagementContext from './context/Management'
import Alert from './components/Alert'
import Routes from './components/Routes'


const App = () => {
    const [role, setRole] = useState(localStorage.getItem('role') || null)
    const [isAuthenticated, setIsAuthenticated] = useState(localStorage.getItem('token') || false)
    const [alert, setAlert] = useState(null)
    const value = { role, setRole, isAuthenticated, setIsAuthenticated, alert, setAlert }
    
    return (
        <ManagementContext.Provider value={value}>
            <BrowserRouter>
                {alert && <Alert alert={alert} setAlert={setAlert} />}
                <Routes />
            </BrowserRouter>
        </ManagementContext.Provider>
    )
}


ReactDOM.render(<App />, document.getElementById('app'))

