import React, { useContext } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import ManagementContext from '../context/Management'
import Home from './Home'
import Logistics from './Logistics'
import Sales from './Sales'


const ProtectedRoute = ({ component: Component, isAuthenticated, path }) => (
    <Route
        path={path}
        render={props => isAuthenticated ? <Component {...props} /> : <Redirect to='/' />}
    />
)

const Routes = () => {
    const { isAuthenticated, role } = useContext(ManagementContext)

    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <ProtectedRoute isAuthenticated={isAuthenticated && role == 'logistics'} path='/logistics' component={Logistics} />
            <ProtectedRoute isAuthenticated={isAuthenticated && role == 'sales'} path='/sales' component={Sales} />
        </Switch>
    )
}

export default Routes