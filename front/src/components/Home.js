import React, { useContext, useEffect } from 'react'
import { Tabs, Form, Input, Select, Button } from 'antd'
import { useHistory } from 'react-router-dom'
import useFetchData from '../hooks/useFetchData'
import ManagementContext from '../context/Management'


const { TabPane } = Tabs
const { Option } = Select


const Home = () => {
	const history = useHistory()
	const { data, loading, sendRequest } = useFetchData()
	const { setIsAuthenticated, setRole } = useContext(ManagementContext)


	useEffect(() => {
		if (data) {
			setIsAuthenticated(data.response.token)
			localStorage.setItem('token', data.response.token)
			setRole(data.response.role)
			localStorage.setItem('role', data.response.role)
			switch (data.response.role) {
				case 'sales':
					history.push('/sales')
					break
				case 'logistics':
					history.push('/logistics')
					break
				default: break
			}
		}
	}, [data])

	const layout = {
		labelCol: { span: 3 },
		wrapperCol: { span: 16 },
	}
	const tailLayout = {
		wrapperCol: { offset: 8, span: 16 },
	}

	const onFinish = (path, values) => {
		sendRequest('POST', path, values)
	}

	return (
		<Tabs defaultActiveKey='1'>
			<TabPane tab='Register' key='1'>
				<Form
					{...layout}
					name='register'
					onFinish={values => onFinish('users/register', values)}
				>
					<Form.Item
						label='Email'
						name='email'
						rules={[{ type: 'email', message: 'Not valid e-mail' }, { required: true, message: 'Please input your email' }]}
					>
						<Input />
					</Form.Item>
					<Form.Item
						label='Password'
						name='password'
						rules={[{ required: true, message: 'Please input your password' }]}
					>
						<Input.Password />
					</Form.Item>
					<Form.Item
						label='Confirm Password'
						name='confirmPassword'
						dependencies={['password']}
						hasFeedback
						rules={[
							{ required: true, message: 'Please confirm your password' },
							({ getFieldValue }) => ({
								validator(rule, value) {
									if (value || getFieldValue('password') === value) {
										return Promise.resolve()
									}
									return Promise.reject('The two passwords that you entered do not match')
								},
							}),
						]}
					>
						<Input.Password />
					</Form.Item>
					<Form.Item name='role' label='Role' rules={[{ required: true }]}>
						{/* This would not be used in a production site */}
						<Select placeholder='This value is only here for the purpose of creating users with roles for this test.'>
							<Option value='sales'>Sales</Option>
							<Option value='logistics'>Logistics</Option>
						</Select>
					</Form.Item>
					<Form.Item {...tailLayout}>
						<Button type='primary' loading={loading} htmlType='submit'>Submit</Button>
					</Form.Item>
				</Form>
			</TabPane>
			<TabPane tab='Login' key='2'>
				<Form
					{...layout}
					name='login'
					onFinish={values => onFinish('users/login', values)}
				>
					<Form.Item
						label='Email'
						name='email'
						rules={[{ required: true, message: 'Please input your username!' }]}
					>
						<Input />
					</Form.Item>
					<Form.Item
						label='Password'
						name='password'
						rules={[{ required: true, message: 'Please input your password!' }]}
					>
						<Input.Password />
					</Form.Item>
					<Form.Item {...tailLayout}>
						<Button type='primary' loading={loading} htmlType='submit'>Submit</Button>
					</Form.Item>
				</Form>
			</TabPane>
		</Tabs>
	)
}


export default Home