import React, { useState, useEffect } from 'react'
import { Space, Card, Form, Input, Button } from 'antd'
import { CheckOutlined } from '@ant-design/icons'
import useFetchData from '../hooks/useFetchData'
import Table from './Table'
import Search from './Search'


const Logistics = () => {
    const [dataSourceRequests, setDataSourceRequests] = useState({ requestInventory: [], count: null })
    const [requestsPagination, setRequestsPagination] = useState({ current: 1, pageSize: 10 })
    const [dataSourceInventory, setDataSourceInventory] = useState({ inventory: [], count: null })
    const [inventoryPagination, setInventoryPagination] = useState({ current: 1, pageSize: 10 })
    const { data, loading, sendRequest } = useFetchData()

    useEffect(() => {
        sendRequest('GET', 'inventory');
        sendRequest('GET', 'requestInventory');
    }, [])

    useEffect(() => {
        if (data && data.response && data.response.inventory) {
            setDataSourceInventory(data.response)
        }

        if (data && data.response && data.response.requestInventory) {
            setDataSourceRequests(data.response)
        }
    }, [data])

    useEffect(() => { sendRequest('GET', 'requestInventory', requestsPagination) }, [requestsPagination])
    useEffect(() => { sendRequest('GET', 'inventory', inventoryPagination) }, [inventoryPagination])

    const onFinish = values => {
        sendRequest('POST', 'inventory', values)
    }

    const onUpdate = row => {
        sendRequest('PATCH', `inventory/${row._id}/logistics`)
    }

    const columnsRequests = [
        { title: 'ID', dataIndex: '_id', key: '_id' },
        { title: 'Name', dataIndex: ['product', 'name'], sorter: true, key: 'name' },
    ]

    const columnsInventory = [
        { title: 'Name', dataIndex: 'name', sorter: true, key: 'name' },
        {
            title: 'In Stock', dataIndex: '', key: 'inStock', sorter: true,
            render: row => <div key={row._id}>{row.inStock.toString()}</div>
        },
        {
            title: 'Item Arrived', dataIndex: '', key: 'update', render: row => (
                <Button
                    key={row._id}
                    type='primary'
                    onClick={e => onUpdate(row)}
                    size='small'
                    disabled={row.inStock}
                    icon={<CheckOutlined />}
                />
            )
        },
    ]

    return (
        <Space direction='vertical'>
            <Card title='Add to Inventory'>
                <Form name='createInventory' onFinish={onFinish}>
                    <Form.Item label='name' name='name' rules={[{ required: true, message: 'Please enter name' }]}                    >
                        <Input />
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' loading={loading} htmlType='submit'>Submit</Button>
                    </Form.Item>
                </Form>
            </Card>
            <Card title='Requests for missing items'>
                <Table
                    columns={columnsRequests}
                    dataSource={dataSourceRequests.requestInventory}
                    pagination={{ ...requestsPagination, total: dataSourceRequests.count }}
                    setPagination={setRequestsPagination}
                />
            </Card>
            <Card title='Create inventory report and update stock'>
                <Search sendRequest={sendRequest} path='inventory' pagination={setInventoryPagination} />
                <Table
                    columns={columnsInventory}
                    dataSource={dataSourceInventory.inventory}
                    pagination={{ ...inventoryPagination, total: dataSourceInventory.count }}
                    setPagination={setInventoryPagination}
                />
            </Card>
        </Space>
    )
}

export default Logistics