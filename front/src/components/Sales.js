import React, { useState, useEffect } from 'react'
import { Space, Card, Button, Table } from 'antd'
import { CheckOutlined } from '@ant-design/icons'
import useFetchData from '../hooks/useFetchData'
import Search from './Search'
import orders from '../../../server/models/orders'


const Sales = () => {
    const [dataSourceInventory, setDataSourceInventory] = useState({ inventory: [], count: null })
    const [inventoryPagination, setInventoryPagination] = useState({ current: 1, pageSize: 10 })
    const [dataSourceOrders, setDataSourceOrders] = useState({ orders: [], count: null })
    const [ordersPagination, setOrdersPagination] = useState({ current: 1, pageSize: 10 })
    const { data, loading, sendRequest } = useFetchData()

    useEffect(() => {
        sendRequest('GET', 'inventory')
        sendRequest('GET', 'orders')
    }, [])

    useEffect(() => {
        if (data && data.response && data.response.inventory) {
            setDataSourceInventory(data.response)
        }

        if (data && data.response && data.response.orders) {
            setDataSourceOrders(data.response)
        }
    }, [data])

    useEffect(() => { sendRequest('GET', 'inventory', inventoryPagination) }, [inventoryPagination])
    useEffect(() => { sendRequest('GET', 'orders', ordersPagination) }, [ordersPagination])

    const onUpdate = row => {
        sendRequest('PATCH', `inventory/${row._id}/sales`)
    }

    const columnsInventory = [
        { title: 'Name', dataIndex: 'name', sorter: true, key: 'name' },
        {
            title: 'In Stock',
            dataIndex: '',
            key: 'inStock',
            sorter: true,
            render: row => <div key={row._id}>{row.inStock.toString()}</div>
        },
        {
            title: 'Item Missing', dataIndex: '', key: 'update', render: row =>
                <Button
                    key={row._id}
                    type='primary'
                    onClick={e => onUpdate(row)}
                    size='small'
                    disabled={row.inStock}
                    icon={<CheckOutlined />}
                />
        },
    ]

    const columnsOrders = [
        { title: 'ID', dataIndex: '_id', key: '_id' },
        { title: 'Name', dataIndex: ['product', 'name'], key: 'name' },
    ]

    return (
        <Space direction='vertical'>
            <Card title='View Inventory Availability'>
                <Table
                    columns={columnsInventory}
                    dataSource={dataSourceInventory.inventory}
                    pagination={{ ...inventoryPagination, total: dataSourceInventory.count }}
                    setPagination={setInventoryPagination}
                />
            </Card>
            <Card title='Create orders report'>
                <Search sendRequest={sendRequest} path='orders' />
                <Table
                    columns={columnsOrders}
                    dataSource={dataSourceOrders.orders}
                    pagination={{ ...ordersPagination, total: dataSourceOrders.count }}
                    setPagination={setOrdersPagination}
                />
            </Card>
        </Space>
    )
}


export default Sales