import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Select, DatePicker } from 'antd'
import moment from 'moment'

const { Option } = Select
const { RangePicker } = DatePicker


const Search = ({ sendRequest, path, pagination }) => {
    const [searchType, setSearchType] = useState(null)

    const onChangeSelect = value => {
        setSearchType(value)
    }

    const onChangeDatePicker = value => {
        sendRequest('GET', `${path}?from=${value.format('L')}&to=${value.format('L')}`, pagination)
    }

    const onChangeRangePicker = values => {
        sendRequest('GET', `${path}?from=${values[0].format('L')}&to=${values[1].format('L')}`, pagination)
    }

    return (
        <>
            <Select placeholder='Select a search method' onChange={onChangeSelect}>
                <Option value='day'>day</Option>
                <Option value='month'>month</Option>
            </Select>
            {searchType == 'day' && <DatePicker onChange={onChangeDatePicker} />}
            {searchType == 'month' && <RangePicker
                ranges={{ 'This Month': [moment().startOf('month'), moment().endOf('month')] }}
                onChange={onChangeRangePicker} />
            }
        </>
    )
}

Search.propTypes = {
    sendRequest: PropTypes.func.isRequired,
    path: PropTypes.string.isRequired,
    pagination: PropTypes.object.isRequired,
}


export default Search