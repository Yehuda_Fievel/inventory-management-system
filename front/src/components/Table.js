import React from 'react'
import { Table } from 'antd'
import PropTypes from 'prop-types'


const TableComponent = props => {

    const onPageChange = (pagination, filters, sorter, extra) => {
        const setPagination = { ...props.pagination, ...pagination }
        if (Object.keys(sorter).length) {
            setPagination.sort = sorter.order == 'ascend' ? sorter.columnKey : `-${sorter.columnKey}`
        }
        props.setPagination(setPagination)
    }

    return (
        <Table
            rowKey={row => row._id}
            columns={props.columns}
            dataSource={props.dataSource}
            onChange={onPageChange}
            pagination={{
                ...props.pagination, // includes the following
                // current: current,
                // pageSize: pageSize,
                // total: total,
                showSizeChanger: true,
                showQuickJumper: true,
            }}

        />
    )
}

TableComponent.propTypes = {
    columns: PropTypes.array.isRequired,
    dataSource: PropTypes.array.isRequired,
    pagination: PropTypes.object.isRequired,
    setPagination: PropTypes.func.isRequired,
}


export default TableComponent