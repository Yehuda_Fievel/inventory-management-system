import { useState, useContext } from 'react'
import axios from 'axios'
import ManagementContext from '../context/Management'
import { SERVER_URL } from '../../config'


const useFetchData = () => {
	const [data, setData] = useState(null)
	const [loading, setLoading] = useState(false)
	const { setAlert } = useContext(ManagementContext)

	async function sendRequest(method, path, postBody) {
		setLoading(true)
		try {
			const request = { method, url: SERVER_URL + path }
			if (method == 'GET' && postBody) {
				request.url += `?skip=${postBody.current - 1}&limit=${postBody.pageSize}`
				request.url += postBody.sort ? `&sort=${postBody.sort}` : ''
			}
			else if (postBody) {
				request.data = postBody
			}
			if (localStorage.getItem('token')) {
				request.headers = { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}
			const result = await axios(request)
			setData(result.data)
			setAlert({ message: 'success', type: 'success' })
		} catch (e) {
			setAlert({ message: e.message, type: 'error' })
		}
		setLoading(false)
	}
	return { data, loading, sendRequest }
}

export default useFetchData