## For Server

- type ` cd server/`
- run `docker-compose up`


## For Front

- type ` cd front/`
- run `npm start`